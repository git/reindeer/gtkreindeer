/*
	This file is part of GTK-Reindeer.

	Copyright (C) 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

[CCode (cprefix = "GtkRen", lower_case_cprefix = "gtk_ren_", cheader_filename = "gtk/gtkren.h")]
namespace Gtk.Ren
{
	public interface Widget : Gtk.Widget
	{
		public Gdk.Ren.Config ren_config { get; construct; }
		public Gdk.Ren.Context ren_context { get; set; }

		public signal void ren_context_changed (Gdk.Ren.Context context);

		public bool is_ren_enabled ();
		public bool check_ren_error () throws GLib.Error;

		public unowned global::Ren.Reindeer ren_begin ();
		public void ren_end ();
		public void ren_swap_buffers ();
	}

	public class DrawingArea : Gtk.DrawingArea, Widget
	{
		public DrawingArea (Gdk.Ren.Config config);
	}
}
