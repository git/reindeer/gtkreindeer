/*
	This file is part of GTK-Reindeer.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GTK_REN_DRAWING_AREA_H
#define GTK_REN_DRAWING_AREA_H

#include <gtk/gtk.h>
#include <gtk/gtkrentypes.h>
#include <gtk/gtkrenwidget.h>
#include <gdk/gdkren.h>

G_BEGIN_DECLS

#define GTK_REN_TYPE_DRAWING_AREA\
	(gtk_ren_drawing_area_get_type ())
#define GTK_REN_DRAWING_AREA(obj)\
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_REN_TYPE_DRAWING_AREA, GtkRenDrawingArea))
#define GTK_REN_DRAWING_AREA_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_CAST ((klass), GTK_REN_TYPE_DRAWING_AREA, GtkRenDrawingAreaClass))
#define GTK_REN_IS_DRAWING_AREA(obj)\
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_REN_TYPE_DRAWING_AREA))
#define GTK_REN_IS_DRAWING_AREA_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_REN_TYPE_DRAWING_AREA))
#define GTK_REN_DRAWING_AREA_GET_CLASS(obj)\
	(G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_REN_TYPE_DRAWING_AREA, GtkRenDrawingAreaClass))

typedef struct _GtkRenDrawingAreaClass GtkRenDrawingAreaClass;

struct _GtkRenDrawingArea
{
	GtkDrawingArea parent;
	GtkRenWidgetProtected prot;
	gulong realize_hack_handler_id;
};

struct _GtkRenDrawingAreaClass
{
	GtkDrawingAreaClass parent;
};

GType
gtk_ren_drawing_area_get_type (void);

GtkRenDrawingArea*
gtk_ren_drawing_area_new (GdkRenConfig *config);

G_END_DECLS

#endif
