/*
	This file is part of GTK-Reindeer.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtkrendrawingarea.h"

enum
{
	PROP_0,
	PROP_REN_CONFIG,
	PROP_REN_CONTEXT,
};

static void
gtk_ren_widget_interface_init (GtkRenWidgetInterface *iface);

static void
gtk_ren_drawing_area_realize (GtkWidget *widget);
static gboolean
gtk_ren_drawing_area_realize_hack (GtkWidget *widget);

static void
gtk_ren_drawing_area_set_property (GObject *object,
	guint property_id, const GValue *value, GParamSpec *pspec);
static void
gtk_ren_drawing_area_get_property (GObject *object,
	guint prop_id, GValue *value, GParamSpec *pspec);


static gboolean
gtk_ren_drawing_area_is_ren_enabled (GtkRenWidget *widget);
static gboolean
gtk_ren_drawing_area_check_ren_error (GtkRenWidget *widget, GError **error);
static RenReindeer*
gtk_ren_drawing_area_ren_begin (GtkRenWidget *widget);
static void
gtk_ren_drawing_area_ren_end (GtkRenWidget *widget);
static void
gtk_ren_drawing_area_ren_swap_buffers (GtkRenWidget *widget);

G_DEFINE_TYPE_WITH_CODE (GtkRenDrawingArea, gtk_ren_drawing_area, GTK_TYPE_DRAWING_AREA,
	G_IMPLEMENT_INTERFACE (GTK_REN_TYPE_WIDGET, gtk_ren_widget_interface_init))
static void
gtk_ren_drawing_area_finalize (GObject *gobject);

static void
gtk_ren_drawing_area_init (GtkRenDrawingArea *self)
{
}

static void
gtk_ren_drawing_area_finalize (GObject *gobject)
{
	GtkRenDrawingArea *self = GTK_REN_DRAWING_AREA (gobject);
	GtkWidget *widget = GTK_WIDGET (gobject);
	_gtk_ren_widget_finalize (widget, &(self->prot));
	G_OBJECT_CLASS (gtk_ren_drawing_area_parent_class)->finalize (gobject);
}

static void
gtk_ren_drawing_area_class_init (GtkRenDrawingAreaClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->finalize = gtk_ren_drawing_area_finalize;
	gobject_class->set_property = gtk_ren_drawing_area_set_property;
	gobject_class->get_property = gtk_ren_drawing_area_get_property;

	g_object_class_override_property (gobject_class,
		PROP_REN_CONFIG, "ren-config");
	g_object_class_override_property (gobject_class,
		PROP_REN_CONTEXT, "ren-context");

	GtkWidgetClass *gtkwidget_class = GTK_WIDGET_CLASS (klass);
	gtkwidget_class->realize = gtk_ren_drawing_area_realize;
}

static void
gtk_ren_widget_interface_init (GtkRenWidgetInterface *iface)
{
	iface->is_ren_enabled = gtk_ren_drawing_area_is_ren_enabled;
	iface->check_ren_error = gtk_ren_drawing_area_check_ren_error;
	iface->ren_begin = gtk_ren_drawing_area_ren_begin;
	iface->ren_end = gtk_ren_drawing_area_ren_end;
	iface->ren_swap_buffers = gtk_ren_drawing_area_ren_swap_buffers;
}


GtkRenDrawingArea*
gtk_ren_drawing_area_new (GdkRenConfig *config)
{
	GtkRenDrawingArea* darea = g_object_new (GTK_REN_TYPE_DRAWING_AREA,
		"ren-config", config, NULL);
	return darea;
}

/*gboolean
gtk_ren_drawing_area_set_ren_context (GtkRenDrawingArea *darea, GdkRenContext *context)
{
	_gtk_ren_widget_set_ren_context ((GtkRenWidget *) darea,
		&(darea->prot), context);
}*/

static void
gtk_ren_drawing_area_realize (GtkWidget *widget)
{
	GtkRenDrawingArea *darea = GTK_REN_DRAWING_AREA (widget);
	GTK_WIDGET_CLASS (gtk_ren_drawing_area_parent_class)->realize (widget);
	g_signal_handler_disconnect (darea, darea->realize_hack_handler_id);
}

static gboolean
gtk_ren_drawing_area_realize_hack (GtkWidget *widget)
{
	GtkRenDrawingArea *darea = GTK_REN_DRAWING_AREA (widget);
	_gtk_ren_widget_realize (widget, &(darea->prot));
	if (gtk_ren_drawing_area_is_ren_enabled ((GtkRenWidget *) darea))
		gdk_ren_context_ren_init (darea->prot.context);
	return FALSE;
}

static void
gtk_ren_drawing_area_set_property (GObject *object,
	guint property_id, const GValue *value, GParamSpec *pspec)
{
	GtkRenDrawingArea *darea = GTK_REN_DRAWING_AREA (object);
	switch (property_id)
	{
		case PROP_REN_CONFIG:
			_gtk_ren_widget_set_ren_config ((GtkRenWidget*) darea,
				&(darea->prot), g_value_get_object (value));
			darea->realize_hack_handler_id =
				g_signal_connect (darea, "configure-event",
				(GCallback) gtk_ren_drawing_area_realize_hack, NULL);
		break;
		case PROP_REN_CONTEXT:
			_gtk_ren_widget_set_ren_context ((GtkRenWidget*) darea,
				&(darea->prot), g_value_get_object (value));
		break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
gtk_ren_drawing_area_get_property (GObject *object,
	guint property_id, GValue *value, GParamSpec *pspec)
{
	GtkRenDrawingArea *darea = GTK_REN_DRAWING_AREA (object);
	switch (property_id)
	{
		case PROP_REN_CONFIG:
			g_value_set_object (value, darea->prot.config);
		break;
		case PROP_REN_CONTEXT:
			g_value_set_object (value, darea->prot.context);
		break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static gboolean
gtk_ren_drawing_area_is_ren_enabled (GtkRenWidget *widget)
{
	GtkRenDrawingArea *darea = GTK_REN_DRAWING_AREA (widget);
	return (
		darea->prot.config != NULL &&
		darea->prot.window != NULL &&
		darea->prot.context != NULL
	);
}

static gboolean
gtk_ren_drawing_area_check_ren_error (GtkRenWidget *widget, GError **error)
{
	GtkRenDrawingArea *darea = GTK_REN_DRAWING_AREA (widget);
	return _gtk_ren_widget_check_ren_error (widget, &(darea->prot), error);
}

static RenReindeer*
gtk_ren_drawing_area_ren_begin (GtkRenWidget *widget)
{
	GtkRenDrawingArea *darea = GTK_REN_DRAWING_AREA (widget);
	return gdk_ren_context_ren_begin (darea->prot.context);
}
static void
gtk_ren_drawing_area_ren_end (GtkRenWidget *widget)
{
	GtkRenDrawingArea *darea = GTK_REN_DRAWING_AREA (widget);
	gdk_ren_context_ren_end (darea->prot.context);
}
static void
gtk_ren_drawing_area_ren_swap_buffers (GtkRenWidget *widget)
{
	GtkRenDrawingArea *darea = GTK_REN_DRAWING_AREA (widget);
	gdk_ren_drawable_swap_buffers ((GdkRenDrawable *) darea->prot.window);
}
