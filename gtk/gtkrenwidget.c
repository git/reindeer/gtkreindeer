/*
	This file is part of GTK-Reindeer.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtkrenwidget.h"

#include <gdk/gdk.h>
#include <gdk/gdkren.h>

static guint context_changed_signal = 0;

static void
gtk_ren_widget_base_init (gpointer g_class);

GType
gtk_ren_widget_get_type (void)
{
	static GType g_define_type_id = 0;
	if (G_UNLIKELY (g_define_type_id == 0))
	{
		static const GTypeInfo g_define_type_info =
		{
			sizeof (GtkRenWidgetInterface),
			(GBaseInitFunc) gtk_ren_widget_base_init,
			(GBaseFinalizeFunc) NULL,
		};
		g_define_type_id = g_type_register_static (G_TYPE_INTERFACE,
			"GtkRenWidget", &g_define_type_info, 0);
		g_type_interface_add_prerequisite (g_define_type_id, GTK_TYPE_WIDGET);
	}
	return g_define_type_id;
}

static void
gtk_ren_widget_base_init (gpointer g_iface)
{
	static gboolean is_initialized = FALSE;
	if (G_UNLIKELY (!is_initialized))
	{
		g_object_interface_install_property (g_iface,
			g_param_spec_object ("ren-config",
				"GtkReindeer configuration",
				"Config to use when Reindeer enabling",
				GDK_REN_TYPE_CONFIG,
				G_PARAM_STATIC_STRINGS | G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
		g_object_interface_install_property (g_iface,
			g_param_spec_object ("ren-context",
				"GtkReindeer context",
				"Context to use when rendering with Reindeer",
				GDK_REN_TYPE_CONTEXT,
				G_PARAM_STATIC_STRINGS | G_PARAM_READWRITE));
		context_changed_signal =
			g_signal_new ("ren-context-changed",
				GTK_REN_TYPE_WIDGET,
				G_SIGNAL_RUN_LAST,
				0, NULL, NULL,
				g_cclosure_marshal_VOID__OBJECT,
				G_TYPE_NONE, 1, GDK_REN_TYPE_CONTEXT);
		is_initialized = TRUE;
	}
}

#if 0
static gboolean
gtk_ren_widget_configure_event (GtkWidget *widget,
	GdkEventConfigure *event, GtkRenWidgetProtected *prot);
#endif

gboolean
_gtk_ren_widget_set_ren_config (GtkRenWidget *widget,
	GtkRenWidgetProtected *prot, GdkRenConfig *config)
{
	g_return_val_if_fail (GTK_IS_WIDGET (widget), FALSE);
	g_return_val_if_fail (GTK_REN_IS_WIDGET (widget), FALSE);
	g_return_val_if_fail (GDK_REN_IS_CONFIG (config), FALSE);
	g_return_val_if_fail (!GTK_WIDGET_NO_WINDOW (widget), FALSE);
	g_return_val_if_fail (!GTK_WIDGET_REALIZED (widget), FALSE);

	/* TODO: Check if the config will actually allow window drawable.
	If the drawable is not window it might still be possible to render to
	whatever it allows and then copy that to the window on swap_buffer.
	However, maybe that code should be somwhere else, such as in a subclass
	of GdkRenDrawable, or maybe backends could handle it by emulation (i.e.
	it implements GdkRenWindow even though the target library cannot handle it
	directly and perhaps the main library can provide basic implementation for
	that). */

	prot->config = g_object_ref (config);

	gtk_widget_set_double_buffered (GTK_WIDGET (widget), FALSE);
	/* TODO: Set appropriate colormap with config visual, possibly with
	gdk_ren_config_get_colormap, or something.  */

	/*g_signal_connect (G_OBJECT (widget), "realize",
		G_CALLBACK (gtk_ren_widget_realize), prot);
	g_signal_connect (G_OBJECT (widget), "configure_event",
		G_CALLBACK (gtk_ren_widget_configure_event), prot);*/

	return TRUE;
}

gboolean
_gtk_ren_widget_set_ren_context (GtkRenWidget *widget,
	GtkRenWidgetProtected *prot, GdkRenContext *context)
{
	g_return_val_if_fail (GDK_REN_IS_CONTEXT (context), FALSE);
	/* If the context is compatible with the config, and the config supports
	changing draw and read for context, we can change here. */
	prot->context = g_object_ref (context);
	g_signal_emit (widget, context_changed_signal, 0, context);

	return TRUE;
}

gboolean
_gtk_ren_widget_check_ren_error (GtkRenWidget *widget,
	GtkRenWidgetProtected *prot, GError **error)
{
	if (prot->error != NULL)
	{
		g_propagate_error (error, prot->error);
		prot->error = NULL;
		return FALSE;
	}
	else
		return TRUE;
}

void
_gtk_ren_widget_finalize (GtkWidget *widget, GtkRenWidgetProtected *prot)
{
	if (prot->config != NULL)
	{
		g_object_unref (prot->config);
		prot->config = NULL;
	}
	if (prot->window != NULL)
	{
		g_object_unref (prot->window);
		prot->window = NULL;
	}
	if (prot->context != NULL)
	{
		g_object_unref (prot->context);
		prot->context = NULL;
	}
}

void
_gtk_ren_widget_realize (GtkWidget *widget, GtkRenWidgetProtected *prot)
{
	GdkWindow *gdkwindow = widget->window;
	_gtk_ren_widget_realize2 (widget, gdkwindow, prot);
}

void
_gtk_ren_widget_realize2 (GtkWidget *widget, GdkWindow *gdkwindow, GtkRenWidgetProtected *prot)
{
	GdkRenConfig *config = prot->config;
	GdkRenWindow *window;

	if (!GTK_WIDGET_NO_WINDOW (widget))
	{
		window = gdk_ren_window_make (config, gdkwindow, &(prot->error));
		if (window == NULL)
			goto FAIL;
		prot->window = window;
	}
	else
	{
		g_set_error_literal (&(prot->error), GDK_REN_ERROR,
			GDK_REN_ERROR_FAILED, "Subwindow not implemented");
		goto FAIL;
	}

	if (prot->context == NULL)
	{
		GdkRenContext *context = gdk_ren_context_make (config,
				NULL, GDK_REN_RENDER_TYPE_ANY,
				(GdkRenDrawable *) window, (GdkRenDrawable *) window, NULL);
		if (context == NULL)
			goto FAIL;
		_gtk_ren_widget_set_ren_context ((GtkRenWidget *) widget, prot, context);
		g_object_unref (context);
	}

	return;

	FAIL:
	if (window != NULL) g_object_unref (window);
	g_prefix_error (&(prot->error), "Cannot make GtkRenWidget Reindeer enabled: ");
	return;
}

gboolean
gtk_ren_widget_is_ren_enabled (GtkRenWidget *widget)
{
	g_return_val_if_fail (GTK_REN_IS_WIDGET (widget), FALSE);
	return GTK_REN_WIDGET_GET_INTERFACE (widget)->is_ren_enabled (widget);
}
gboolean
gtk_ren_widget_check_ren_error (GtkRenWidget *widget, GError **error)
{
	g_return_val_if_fail (GTK_REN_IS_WIDGET (widget), FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);
	return GTK_REN_WIDGET_GET_INTERFACE (widget)->check_ren_error (widget, error);
}

RenReindeer*
gtk_ren_widget_ren_begin (GtkRenWidget *widget)
{
	g_return_val_if_fail (GTK_REN_IS_WIDGET (widget), NULL);
	return GTK_REN_WIDGET_GET_INTERFACE (widget)->ren_begin (widget);
}
void
gtk_ren_widget_ren_end (GtkRenWidget *widget)
{
	g_return_if_fail (GTK_REN_IS_WIDGET (widget));
	GTK_REN_WIDGET_GET_INTERFACE (widget)->ren_end (widget);
}
void
gtk_ren_widget_ren_swap_buffers (GtkRenWidget *widget)
{
	g_return_if_fail (GTK_REN_IS_WIDGET (widget));
	GTK_REN_WIDGET_GET_INTERFACE (widget)->ren_swap_buffers (widget);
}

#if 0
static gboolean
gtk_ren_widget_configure_event (GtkWidget *widget,
	GdkEventConfigure *event, GtkRenWidgetProtected *prot)
{
	/* Update viewport if necessary (i.e. NO_WINDOW == TRUE). */
	return FALSE;
}
#endif
