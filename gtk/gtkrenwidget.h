/*
	This file is part of GTK-Reindeer.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GTK_REN_WIDGET_H
#define GTK_REN_WIDGET_H

#include <gdk/gdkrentypes.h>
#include <gtk/gtkrentypes.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <ren/types.h>

G_BEGIN_DECLS

typedef struct _GtkRenWidgetProtected
{
	GdkRenConfig *config;
	GdkRenWindow *window;
	GdkRenContext *context;
	GError *error;
} GtkRenWidgetProtected;

#define GTK_REN_TYPE_WIDGET\
	(gtk_ren_widget_get_type ())
#define GTK_REN_WIDGET(obj)\
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_REN_TYPE_WIDGET, GtkRenWidget))
#define GTK_REN_IS_WIDGET(obj)\
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_REN_TYPE_WIDGET))
#define GTK_REN_WIDGET_GET_INTERFACE(inst)\
	(G_TYPE_INSTANCE_GET_INTERFACE ((inst), GTK_REN_TYPE_WIDGET, GtkRenWidgetInterface))

typedef struct _GtkRenWidgetInterface GtkRenWidgetInterface;

struct _GtkRenWidgetInterface
{
	GTypeInterface parent;

	gboolean (* is_ren_enabled) (GtkRenWidget *widget);
	gboolean (* check_ren_error) (GtkRenWidget *widget, GError **error);

	RenReindeer* (* ren_begin) (GtkRenWidget *widget);
	void (* ren_end) (GtkRenWidget *widget);
	void (* ren_swap_buffers) (GtkRenWidget *widget);
};

GType
gtk_ren_widget_get_type (void);

#if 0
#define GTK_REN_DEFINE_TYPE(TN, t_n, T_P)\
	static void t_n##_ren_widget_iface_init(GtkRenWidgetInterface *iface)\
	{\
		static gboolean initialized = FALSE;\
		if (!initialized) {\
			iface->\
		}\
	}\
	G_DEFINE_TYPE_WITH_CODE (TN, t_n, T_P,\
	G_IMPLEMENT_INTERFACE(GTK_REN_TYPE_WIDGET, t_n##_ren_widget_iface_init))

#define GTK_REN_WIDGET_IMPL_INIT(self)\
	self->renpriv = _gtk_widget_ren_enable (self,\
	/* TODO: Need construction properties */)
#endif

void
_gtk_ren_widget_finalize (GtkWidget *widget, GtkRenWidgetProtected *prot);

void
_gtk_ren_widget_realize (GtkWidget *widget, GtkRenWidgetProtected *prot);
void
_gtk_ren_widget_realize2 (GtkWidget *widget, GdkWindow *gdkwindow, GtkRenWidgetProtected *prot);

gboolean
_gtk_ren_widget_set_ren_config (GtkRenWidget *widget,
	GtkRenWidgetProtected *prot, GdkRenConfig *config);

gboolean
_gtk_ren_widget_set_ren_context (GtkRenWidget *widget,
	GtkRenWidgetProtected *prot, GdkRenContext *context);

gboolean
_gtk_ren_widget_check_ren_error (GtkRenWidget *widget,
	GtkRenWidgetProtected *prot, GError **error);

gboolean
gtk_ren_widget_is_ren_enabled (GtkRenWidget *widget);
gboolean
gtk_ren_widget_check_ren_error (GtkRenWidget *widget, GError **error);

RenReindeer*
gtk_ren_widget_ren_begin (GtkRenWidget *widget);
void
gtk_ren_widget_ren_end (GtkRenWidget *widget);
void
gtk_ren_widget_ren_swap_buffers (GtkRenWidget *widget);

/*
GdkRenConfig*
gtk_ren_widget_get_ren_config (GtkRenWidget *widget);

GdkRenContext*
gtk_ren_widget_get_ren_context (GtkRenWidget *widget);
void
gtk_ren_widget_set_ren_context (GtkRenWidget *widget, GdkRenContext *context);

GdkRenDrawable*
gtk_ren_widget_get_ren_window (GtkRenWidget *widget);
*/

G_END_DECLS

#endif
