/*
	This file is part of GTK-Reindeer.

	Copyright (C) 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <glib.h>
#include <gtk/gtkren.h>

#define IMMEDIATE 0
/* TODO: This should as soon as possible be replaced with something that checks
what the backend prefers.  */

typedef struct Vertex Vertex;
struct Vertex
{
	struct
	{
		ren_sfloat x;
		ren_sfloat y;
		ren_sfloat z;
	} coord;
	struct
	{
		ren_sfloat x;
		ren_sfloat y;
		ren_sfloat z;
	} normal;
};

static const Vertex vertices[] =
{
	/* Negative X */
	{{-1.0f, +1.0f, -1.0f}, {-1.0f, 0.0f, 0.0f}},
	{{-1.0f, +1.0f, +1.0f}, {-1.0f, 0.0f, 0.0f}},
	{{-1.0f, -1.0f, +1.0f}, {-1.0f, 0.0f, 0.0f}},
	{{-1.0f, -1.0f, -1.0f}, {-1.0f, 0.0f, 0.0f}},
	/* Negative Y */
	{{-1.0f, -1.0f, -1.0f}, {0.0f, -1.0f, 0.0f}},
	{{-1.0f, -1.0f, +1.0f}, {0.0f, -1.0f, 0.0f}},
	{{+1.0f, -1.0f, +1.0f}, {0.0f, -1.0f, 0.0f}},
	{{+1.0f, -1.0f, -1.0f}, {0.0f, -1.0f, 0.0f}},
	/* Negative Z */
	{{-1.0f, +1.0f, -1.0f}, {0.0f, 0.0f, -1.0f}},
	{{-1.0f, -1.0f, -1.0f}, {0.0f, 0.0f, -1.0f}},
	{{+1.0f, -1.0f, -1.0f}, {0.0f, 0.0f, -1.0f}},
	{{+1.0f, +1.0f, -1.0f}, {0.0f, 0.0f, -1.0f}},
	/* Positive X */
	{{+1.0f, -1.0f, -1.0f}, {+1.0f, 0.0f, 0.0f}},
	{{+1.0f, -1.0f, +1.0f}, {+1.0f, 0.0f, 0.0f}},
	{{+1.0f, +1.0f, +1.0f}, {+1.0f, 0.0f, 0.0f}},
	{{+1.0f, +1.0f, -1.0f}, {+1.0f, 0.0f, 0.0f}},
	/* Positive Y */
	{{+1.0f, +1.0f, -1.0f}, {0.0f, +1.0f, 0.0f}},
	{{+1.0f, +1.0f, +1.0f}, {0.0f, +1.0f, 0.0f}},
	{{-1.0f, +1.0f, +1.0f}, {0.0f, +1.0f, 0.0f}},
	{{-1.0f, +1.0f, -1.0f}, {0.0f, +1.0f, 0.0f}},
	/* Positive Z */
	{{-1.0f, -1.0f, +1.0f}, {0.0f, 0.0f, +1.0f}},
	{{-1.0f, +1.0f, +1.0f}, {0.0f, 0.0f, +1.0f}},
	{{+1.0f, +1.0f, +1.0f}, {0.0f, 0.0f, +1.0f}},
	{{+1.0f, -1.0f, +1.0f}, {0.0f, 0.0f, +1.0f}}
};

static const ren_uint32 indices[] =
{
	0,   1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11,
	12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
};

static RenVectorArray *coord_array = NULL;
static RenVectorArray *normal_array = NULL;
static RenIndexArray *index_array = NULL;
static RenMaterial *material = NULL;
static RenObject *object = NULL;
static RenMatrix *proj_matrix = NULL;
static RenMatrix *view_matrix = NULL;
static RenMatrix *light_matrix = NULL;
static RenLight *light = NULL;

static gboolean
configure (GtkWidget *widget, GdkEventConfigure *event);

static gboolean
expose (GtkWidget *widget, GdkEventExpose *event);

static void
set_color (RenColor *color,
	ren_sfloat r, ren_sfloat g, ren_sfloat b, ren_sfloat a);

static void
make_proj_matrix (ren_sfloat matrix[16], ren_sfloat ratio);

static void
make_view_matrix (ren_sfloat matrix[16]);

static void
make_light_matrix (ren_sfloat matrix[16]);

static char* backend_opt = NULL;

static GOptionEntry entries[] =
{
  { "backend", 'b', 0, G_OPTION_ARG_STRING, &backend_opt, "Reindeer backend to use", "BACKEND" },
  { NULL }
};

int
main (int argc, char *argv[])
{
	gint i;
	gint exitcode = EXIT_FAILURE;
	GError *error = NULL;
	RenError *renerror = NULL;
	GOptionContext *optcontext = NULL;
	GdkRenBackend *backend = NULL;
	GdkRenConfig *config = NULL;
	RenDataBlock *data_block = NULL;
	void *data = NULL;
	RenColor *white = NULL, *bright = NULL, *dark = NULL;
	RenTemplate *tmplt = NULL;
	GtkWindow *window = NULL;
	GtkRenDrawingArea *darea = NULL;

	g_type_init ();
	optcontext = g_option_context_new (NULL);
	g_option_context_add_main_entries (optcontext, entries, NULL);
	g_option_context_add_group (optcontext, gtk_get_option_group (TRUE));
	if (!g_option_context_parse (optcontext, &argc, &argv, &error))
	{
		g_printerr ("%s\n", error->message);
		goto EXIT;
	}
	if (backend_opt == NULL)
		backend_opt = g_strdup ("opengl");
	if (!ren_library_init (&renerror))
	{
		g_printerr ("%s\n", renerror->message);
		goto EXIT;
	}
	if (!gdk_ren_init (&error))
	{
		g_printerr ("%s\n", error->message);
		goto EXIT;
	}

	g_print ("Using Reindeer backend `%s'\n", backend_opt);
	backend = gdk_ren_backend_lookup (backend_opt, &error);
	if (backend == NULL)
	{
		g_printerr ("%s\n", error->message);
		goto EXIT;
	}
	config = gdk_ren_config_find (backend, NULL,
		&GDK_REN_CONFIG_ATTR_DEFAULT, gdk_ren_config_attr_compare_default, &error);
	if (config == NULL)
	{
		g_printerr ("%s\n", error->message);
		goto EXIT;
	}

	/* Build Colors */
	white = ren_color_new (REN_COLOR_FORMAT_RGBA, REN_TYPE_SFLOAT);
	set_color (white, 1.0f, 1.0f, 1.0f, 1.0f);
	bright = ren_color_new (REN_COLOR_FORMAT_RGBA, REN_TYPE_SFLOAT);
	set_color (bright, 0.6f, 0.0f, 0.8f, 1.0f);
	dark = ren_color_new (REN_COLOR_FORMAT_RGBA, REN_TYPE_SFLOAT);
	set_color (dark, 0.15f, 0.0f, 0.2f, 1.0f);

	/* Build DataBlock for vertices */
	ren_size n_vertices = G_N_ELEMENTS (vertices);
	data_block = ren_data_block_new (sizeof (vertices), REN_USAGE_STATIC);
	data = ren_data_block_begin_edit (data_block);
	memcpy (data, vertices, sizeof (vertices));
	ren_data_block_changed (data_block, 0, 0);
	ren_data_block_end_edit (data_block);

	/* Build coordinates VectorArray */
	coord_array = ren_vector_array_new (
		REN_TYPE_SFLOAT, 3, data_block, G_STRUCT_OFFSET (Vertex, coord),
		n_vertices, sizeof (Vertex));

	/* Build normals VectorArray */
	normal_array = ren_vector_array_new (
		REN_TYPE_SFLOAT, 3, data_block, G_STRUCT_OFFSET (Vertex, normal),
		n_vertices, sizeof (Vertex));

	ren_data_block_unref (data_block);

	/* Build DataBlock for indices */
	ren_size n_indices = G_N_ELEMENTS (indices);
	data_block = ren_data_block_new (sizeof (indices), REN_USAGE_STATIC);
	data = ren_data_block_begin_edit (data_block);
	memcpy (data, indices, sizeof (indices));
	ren_data_block_changed (data_block, 0, 0);
	ren_data_block_end_edit (data_block);

	/* Build IndexArray */
	index_array = ren_index_array_new (
		REN_TYPE_UINT32, data_block, 0, n_indices);

	ren_data_block_unref (data_block);

	/* Build Material */
	material = ren_material_new ();
	ren_material_ambient (material, dark);
	ren_material_diffuse (material, bright);

	/* Build Template */
	tmplt = ren_template_new (index_array);
	for (i = 0; i < 6; ++i)
	{
		ren_template_new_mode (tmplt);
		ren_template_material (tmplt, REN_FACE_BOTH, i);
		ren_template_primitive (tmplt, REN_PRIMITIVE_QUADS, i*4, 1);
	}
	ren_template_build (tmplt);

	/* Build Object */
	object = ren_object_new (tmplt, coord_array);
	ren_object_begin_edit (object);
	ren_object_normal_array (object, normal_array);
	for (i = 0; i < 6; ++i)
		ren_object_material (object, i, material);
	ren_object_end_edit (object);

	/* Build Light */
	light = ren_light_new (REN_LIGHT_TYPE_POINT_LIGHT);
	ren_light_ambient (light, white);
	ren_light_diffuse (light, white);
	ren_light_specular (light, white);

	/* Build Matrices */
	proj_matrix = ren_matrix_new (4, 4, REN_TYPE_SFLOAT, FALSE);
	view_matrix = ren_matrix_new (4, 4, REN_TYPE_SFLOAT, FALSE);
	data = ren_matrix_begin_edit (view_matrix);
	make_view_matrix (data);
	ren_matrix_end_edit (view_matrix);
	light_matrix = ren_matrix_new (4, 4, REN_TYPE_SFLOAT, FALSE);
	data = ren_matrix_begin_edit (light_matrix);
	make_light_matrix (data);
	ren_matrix_end_edit (light_matrix);

	/* Remove references to things we won't use directly anymore */
	ren_color_unref (white);
	ren_color_unref (bright);
	ren_color_unref (dark);
	ren_template_unref (tmplt);

	/* Build GUI */
	window = (GtkWindow*) gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (window, "GTK-Reindeer Cube Example");
	gtk_window_set_default_size (window, 640, 480);
	g_signal_connect (window, "destroy", (GCallback) gtk_main_quit, NULL);

	darea = gtk_ren_drawing_area_new (config);
	g_signal_connect (darea, "configure-event", (GCallback) configure, NULL);
	g_signal_connect (darea, "expose-event", (GCallback) expose, NULL);
	gtk_container_add ((GtkContainer*) window, (GtkWidget*) darea);

	gtk_widget_show_all ((GtkWidget*) window);
	if (!gtk_ren_widget_check_ren_error ((GtkRenWidget *) darea, &error))
	{
		g_printerr ("%s\n", error->message);
		goto EXIT;
	}

	/* Main loop */
	gtk_main ();
	exitcode = EXIT_SUCCESS;

	/* Exiting */
	EXIT:
	if (error != NULL) g_error_free (error);
	if (renerror != NULL) ren_error_free (renerror);
	if (optcontext != NULL) g_option_context_free (optcontext);
	if (backend != NULL) gdk_ren_backend_unuse (backend);
	if (config != NULL) g_object_unref (config);
	if (coord_array != NULL) ren_vector_array_unref (coord_array);
	if (normal_array != NULL) ren_vector_array_unref (normal_array);
	if (index_array != NULL) ren_index_array_unref (index_array);
	if (material != NULL) ren_material_unref (material);
	if (object != NULL) ren_object_unref (object);
	if (light != NULL) ren_light_unref (light);
	if (proj_matrix != NULL) ren_matrix_unref (proj_matrix);
	if (view_matrix != NULL) ren_matrix_unref (view_matrix);
	if (light_matrix != NULL) ren_matrix_unref (light_matrix);
	if (backend_opt != NULL) g_free (backend_opt);

	gdk_ren_exit ();
	ren_library_exit ();
	exit (exitcode);
}

static gboolean
configure (GtkWidget *widget, GdkEventConfigure *event)
{
	GtkRenWidget *renwidget = (GtkRenWidget *) widget;
	RenReindeer *r = gtk_ren_widget_ren_begin (renwidget);
	ren_viewport (r, event->x, event->y, event->width, event->height);
	ren_sfloat ratio =
		((ren_sfloat) event->width) / ((ren_sfloat) event->height);
	void *data = ren_matrix_begin_edit (proj_matrix);
	make_proj_matrix (data, ratio);
	ren_matrix_end_edit (proj_matrix);
	gtk_ren_widget_ren_end (renwidget);

	return FALSE;
}

static gboolean
expose (GtkWidget *widget, GdkEventExpose *event)
{
	GtkRenWidget *renwidget = (GtkRenWidget *) widget;
	RenReindeer *r = gtk_ren_widget_ren_begin (renwidget);

#if IMMEDIATE
	ren_state_keep (r, REN_STATE_LIGHTS | REN_STATE_INDEX_ARRAY |
		REN_STATE_COORD_ARRAY | REN_STATE_NORMAL_ARRAY | REN_STATE_MATERIALS);
	ren_coord_array_bind (r, coord_array);
	ren_normal_array_bind (r, normal_array);
	ren_index_array_bind (r, index_array);
#else
	ren_lighting_enable (r);
#endif

	ren_clear (r, REN_BUFFER_COLOR | REN_BUFFER_DEPTH);

	ren_transform_mode (r, REN_TRANSFORM_MODE_PROJECTION);
	ren_transform_set (r, proj_matrix);

	ren_transform_mode (r, REN_TRANSFORM_MODE_MODELVIEW);
	ren_transform_set (r, view_matrix);

	ren_transform_push (r);
	ren_transform_mul (r, light_matrix);
	ren_light_bind (r, 1, &light);
	ren_transform_pop (r);

#if IMMEDIATE
	ren_material_bind (r, material, material);
	ren_primitive (r, REN_PRIMITIVE_QUADS, 0, 6);
#else
	ren_object_render (r, object);
#endif

	ren_finish (r);

	gtk_ren_widget_ren_end (renwidget);
	gtk_ren_widget_ren_swap_buffers (renwidget);

	return FALSE;
}

static void
set_color (RenColor *color,
	ren_sfloat r, ren_sfloat g, ren_sfloat b, ren_sfloat a)
{
	ren_sfloat *data = ren_color_begin_edit (color);
	data[0] = r;
	data[1] = g;
	data[2] = b;
	data[3] = a;
	ren_color_end_edit (color);
}

/* Makes an orthographic projection matrix */
static void
make_proj_matrix (ren_sfloat matrix[16], ren_sfloat ratio)
{
	/* column X */
	matrix[0x0] = +0.5f / ratio;
	matrix[0x1] =  0.0f;
	matrix[0x2] =  0.0f;
	matrix[0x3] =  0.0f;
	/* column Y */
	matrix[0x4] =  0.0f;
	matrix[0x5] = +0.5f;
	matrix[0x6] =  0.0f;
	matrix[0x7] =  0.0f;
	/* column Z */
	matrix[0x8] =  0.0f;
	matrix[0x9] =  0.0f;
	matrix[0xa] = -0.1f;
	matrix[0xb] =  0.0f;
	/* column W */
	matrix[0xc] =  0.0f;
	matrix[0xd] =  0.0f;
	matrix[0xe] =  0.0f;
	matrix[0xf] = +1.0f;
}

/* See all sides */
static void
make_view_matrix (ren_sfloat matrix[16])
{
	ren_sfloat side = sqrt(1.0f/3.0f);

	/* column X */
	matrix[0x0] = +side;
	matrix[0x1] = -side;
	matrix[0x2] = -side;
	matrix[0x3] =  0.0f;
	/* column Y */
	matrix[0x4] = +side;
	matrix[0x5] = +side;
	matrix[0x6] = +side;
	matrix[0x7] =  0.0f;
	/* column Z */
	matrix[0x8] =  0.0f;
	matrix[0x9] = +0.6f;
	matrix[0xa] = -0.8f;
	matrix[0xb] =  0.0f;
	/* column W */
	matrix[0xc] =  0.0f;
	matrix[0xd] =  0.0f;
	matrix[0xe] =  0.0f;
	matrix[0xf] = +1.0f;
}

/* Light position */
static void
make_light_matrix (ren_sfloat matrix[16])
{
	/* column X */
	matrix[0x0] = +1.0f;
	matrix[0x1] =  0.0f;
	matrix[0x2] =  0.0f;
	matrix[0x3] =  0.0f;
	/* column Y */
	matrix[0x4] =  0.0f;
	matrix[0x5] = +1.0f;
	matrix[0x6] =  0.0f;
	matrix[0x7] =  0.0f;
	/* column Z */
	matrix[0x8] =  0.0f;
	matrix[0x9] =  0.0f;
	matrix[0xa] = +1.0f;
	matrix[0xb] =  0.0f;
	/* column W */
	matrix[0xc] = +4.0f;
	matrix[0xd] = -2.0f;
	matrix[0xe] = +8.0f;
	matrix[0xf] = +1.0f;
}
