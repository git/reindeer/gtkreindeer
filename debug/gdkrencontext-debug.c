/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gdkrencontext-debug.h"
#include "gdkrenconfig-debug.h"
#include "gdkrenwindow-debug.h"

#include <gdk/gdk.h>
#include <gdk/gdkren.h>
#include <ren/ren.h>

static void
gdk_ren_context_debug_set_draw (GdkRenContext *context, GdkRenDrawable *draw);
static void
gdk_ren_context_debug_set_read (GdkRenContext *context, GdkRenDrawable *read);
static RenReindeer*
gdk_ren_context_debug_ren_begin (GdkRenContext *context);
static void
gdk_ren_context_debug_ren_end (GdkRenContext *context);

static gint
gdk_ren_context_debug_get_swap_interval (GdkRenContext *rencontext);
static gboolean
gdk_ren_context_debug_set_swap_interval (GdkRenContext *rencontext, gint interval);

G_DEFINE_DYNAMIC_TYPE (GdkRenContextDebug, gdk_ren_context_debug, GDK_REN_TYPE_CONTEXT)

void
gdk_ren_context_debug_type_register (GTypeModule *type_module)
{
	gdk_ren_context_debug_register_type (type_module);
}

static void
gdk_ren_context_debug_init (GdkRenContextDebug *self)
{
}

static void
gdk_ren_context_debug_class_init (GdkRenContextDebugClass *klass)
{
	GdkRenContextClass *rencontext_class = GDK_REN_CONTEXT_CLASS (klass);
	rencontext_class->set_draw = gdk_ren_context_debug_set_draw;
	rencontext_class->set_read = gdk_ren_context_debug_set_read;
	rencontext_class->ren_begin = gdk_ren_context_debug_ren_begin;
	rencontext_class->ren_end = gdk_ren_context_debug_ren_end;
	rencontext_class->get_swap_interval = gdk_ren_context_debug_get_swap_interval;
	rencontext_class->set_swap_interval = gdk_ren_context_debug_set_swap_interval;
}

static void
gdk_ren_context_debug_class_finalize (GdkRenContextDebugClass *klass)
{
}

GdkRenContext*
gdk_ren_context_debug_make (GdkRenConfig *renconfig,
	GdkRenContext *share, GdkRenRenderType render_type,
	GdkRenDrawable *draw, GdkRenDrawable *read, GError **error)
{
	g_return_val_if_fail (draw == NULL || GDK_REN_IS_WINDOW_DEBUG (draw), NULL);
	g_return_val_if_fail (read == NULL || GDK_REN_IS_WINDOW_DEBUG (read), NULL);
	g_return_val_if_fail (GDK_REN_IS_CONFIG_DEBUG (renconfig), NULL);
	g_return_val_if_fail (share == NULL || GDK_REN_IS_CONTEXT_DEBUG (share), NULL);
	g_return_val_if_fail (render_type & GDK_REN_RENDER_TYPE_RGBA ||
		render_type & GDK_REN_RENDER_TYPE_INDEX, NULL);

	GdkRenBackend *backend = gdk_ren_config_get_backend (renconfig);
	GdkRenConfigDebug *config = (GdkRenConfigDebug *) (renconfig);

	RenBackend *renbackend = gdk_ren_backend_get_ren_backend (backend);
	RenError *rerror = NULL;
	RenReindeer *r = ren_reindeer_new (renbackend, &rerror);
	if (r == NULL)
	{
		g_set_error_literal (error, GDK_REN_ERROR,
			GDK_REN_ERROR_FAILED, rerror->message);
		ren_error_free (rerror);
		return NULL;
	}

	GdkRenContextDebug *context =
		g_object_new (GDK_REN_TYPE_CONTEXT_DEBUG, NULL);
	GdkRenContext *rencontext = (GdkRenContext *) (context);
	rencontext->config = g_object_ref (renconfig);
	rencontext->share = (share != NULL) ? g_object_ref (share) : NULL;
	rencontext->render_type = render_type;
	rencontext->draw = (draw != NULL) ? g_object_ref (draw) : NULL;
	rencontext->read = (read != NULL) ? g_object_ref (read) : NULL;
	rencontext->r = r;

	return rencontext;
}

static void
gdk_ren_context_debug_set_draw (GdkRenContext *rencontext, GdkRenDrawable *draw)
{
	g_return_if_fail (GDK_REN_IS_CONTEXT_DEBUG (rencontext));
	g_return_if_fail (GDK_REN_IS_WINDOW_DEBUG (draw));
	if (rencontext->draw != NULL)
		g_object_unref (rencontext->draw);
	rencontext->draw = (draw != NULL) ? g_object_ref (draw) : NULL;
}

static void
gdk_ren_context_debug_set_read (GdkRenContext *rencontext, GdkRenDrawable *read)
{
	g_return_if_fail (GDK_REN_IS_CONTEXT_DEBUG (rencontext));
	g_return_if_fail (GDK_REN_IS_WINDOW_DEBUG (read));
	if (rencontext->read != NULL)
		g_object_unref (rencontext->read);
	rencontext->read = (read != NULL) ? g_object_ref (read) : NULL;
}

static RenReindeer*
gdk_ren_context_debug_ren_begin (GdkRenContext *rencontext)
{
	g_return_val_if_fail (GDK_REN_IS_CONTEXT_DEBUG (rencontext), NULL);
	return rencontext->r;
}

static void
gdk_ren_context_debug_ren_end (GdkRenContext *rencontext)
{
	g_return_if_fail (GDK_REN_IS_CONTEXT_DEBUG (rencontext));
}

static gint
gdk_ren_context_debug_get_swap_interval (GdkRenContext *rencontext)
{
	g_return_val_if_fail (GDK_REN_IS_CONTEXT_DEBUG (rencontext), -1);
	/* TODO: Check if current context. */
	return -1;
}

static gboolean
gdk_ren_context_debug_set_swap_interval (GdkRenContext *rencontext, gint interval)
{
	g_return_val_if_fail (GDK_REN_IS_CONTEXT_DEBUG (rencontext), FALSE);
	/* TODO: Check if current context. */
	return FALSE;
}
