/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GDK_REN_CONTEXT_DEBUG_H
#define GDK_REN_CONTEXT_DEBUG_H

#include <gdk/gdkrencontext.h>

G_BEGIN_DECLS

#define GDK_REN_TYPE_CONTEXT_DEBUG\
	(gdk_ren_context_debug_get_type ())
#define GDK_REN_CONTEXT_DEBUG(obj)\
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), GDK_REN_TYPE_CONTEXT_DEBUG, GdkRenContextDebug))
#define GDK_REN_CONTEXT_DEBUG_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_CAST ((klass), GDK_REN_TYPE_CONTEXT_DEBUG, GdkRenContextDebugClass))
#define GDK_REN_IS_CONTEXT_DEBUG(obj)\
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GDK_REN_TYPE_CONTEXT_DEBUG))
#define GDK_REN_IS_CONTEXT_DEBUG_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_TYPE ((klass), GDK_REN_TYPE_CONTEXT_DEBUG))
#define GDK_REN_CONTEXT_DEBUG_GET_CLASS(obj)\
	(G_TYPE_INSTANCE_GET_CLASS ((obj), GDK_REN_TYPE_CONTEXT_DEBUG, GdkRenContextDebugClass))

typedef struct GdkRenContextDebug GdkRenContextDebug;
typedef struct GdkRenContextDebugClass GdkRenContextDebugClass;

struct GdkRenContextDebug
{
	GdkRenContext parent;
};

struct GdkRenContextDebugClass
{
	GdkRenContextClass parent;
};

GType
gdk_ren_context_debug_get_type (void);

G_END_DECLS

#endif
