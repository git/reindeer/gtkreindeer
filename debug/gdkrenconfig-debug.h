/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GDK_REN_CONFIG_DEBUG_H
#define GDK_REN_CONFIG_DEBUG_H

#include <gdk/gdkrenconfig.h>

G_BEGIN_DECLS

#define GDK_REN_TYPE_CONFIG_DEBUG\
	(gdk_ren_config_debug_get_type ())
#define GDK_REN_CONFIG_DEBUG(obj)\
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), GDK_REN_TYPE_CONFIG_DEBUG, GdkRenConfigDebug))
#define GDK_REN_CONFIG_DEBUG_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_CAST ((klass), GDK_REN_TYPE_CONFIG_DEBUG, GdkRenConfigDebugClass))
#define GDK_REN_IS_CONFIG_DEBUG(obj)\
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GDK_REN_TYPE_CONFIG_DEBUG))
#define GDK_REN_IS_CONFIG_DEBUG_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_TYPE ((klass), GDK_REN_TYPE_CONFIG_DEBUG))
#define GDK_REN_CONFIG_DEBUG_GET_CLASS(obj)\
	(G_TYPE_INSTANCE_GET_CLASS ((obj), GDK_REN_TYPE_CONFIG_DEBUG, GdkRenConfigDebugClass))

typedef struct GdkRenConfigDebug GdkRenConfigDebug;
typedef struct GdkRenConfigDebugClass GdkRenConfigDebugClass;

struct GdkRenConfigDebug
{
	GdkRenConfig parent;
};

struct GdkRenConfigDebugClass
{
	GdkRenConfigClass parent;
};

GType
gdk_ren_config_debug_get_type (void);

G_END_DECLS

#endif
