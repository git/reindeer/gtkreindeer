/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gdkrenconfig-debug.h"

#include <gdk/gdk.h>

G_DEFINE_DYNAMIC_TYPE (GdkRenConfigDebug, gdk_ren_config_debug, GDK_REN_TYPE_CONFIG)

void
gdk_ren_config_debug_type_register (GTypeModule *type_module)
{
	gdk_ren_config_debug_register_type (type_module);
}

static void
gdk_ren_config_debug_init (GdkRenConfigDebug *self)
{
}

static void
gdk_ren_config_debug_class_init (GdkRenConfigDebugClass *klass)
{
}

static void
gdk_ren_config_debug_class_finalize (GdkRenConfigDebugClass *klass)
{
}

GdkRenConfig*
gdk_ren_config_debug_find (GdkRenBackend *backend, GdkScreen *screen,
	const GdkRenConfigAttr *attr, GdkRenConfigAttrCompareFunc compare,
	GError **error)
{
	if (screen == NULL)
		screen = gdk_screen_get_default ();
	else
		g_return_val_if_fail (GDK_IS_SCREEN (screen), NULL);
	if (attr == NULL)
		attr = &GDK_REN_CONFIG_ATTR_DEFAULT;

	GdkRenConfigAttr *attributes = g_new (GdkRenConfigAttr, 1);
	*attributes = *attr;

	GdkRenConfigDebug *config =
		g_object_new (GDK_REN_TYPE_CONFIG_DEBUG, NULL);
	GdkRenConfig *renconfig = (GdkRenConfig *) (config);
	renconfig->attributes = attributes;
	renconfig->backend = backend;
	renconfig->screen = g_object_ref (screen);

	return renconfig;
}
