/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gdkrenwindow-debug.h"
#include "gdkrenconfig-debug.h"

#include <gdk/gdk.h>

static void
gdk_ren_window_debug_swap_buffers (GdkRenDrawable *drawable);

G_DEFINE_DYNAMIC_TYPE (GdkRenWindowDebug, gdk_ren_window_debug, GDK_REN_TYPE_WINDOW)

void
gdk_ren_window_debug_type_register (GTypeModule *type_module)
{
	gdk_ren_window_debug_register_type (type_module);
}

static void
gdk_ren_window_debug_init (GdkRenWindowDebug *self)
{
}

static void
gdk_ren_window_debug_class_init (GdkRenWindowDebugClass *klass)
{
	GdkRenDrawableClass *gdk_ren_drawable_class = GDK_REN_DRAWABLE_CLASS (klass);
	gdk_ren_drawable_class->swap_buffers = gdk_ren_window_debug_swap_buffers;
}

static void
gdk_ren_window_debug_class_finalize (GdkRenWindowDebugClass *klass)
{
}

GdkRenWindow*
gdk_ren_window_debug_make (GdkRenConfig *renconfig, GdkWindow *gdkwindow,
	GError **error)
{
	g_return_val_if_fail (GDK_REN_IS_CONFIG_DEBUG (renconfig), NULL);
	g_return_val_if_fail (GDK_IS_WINDOW (gdkwindow), NULL);

	GdkRenWindowDebug *window = g_object_new (GDK_REN_TYPE_WINDOW_DEBUG, NULL);
	GdkRenWindow *renwindow = GDK_REN_WINDOW (window);
	renwindow->config = g_object_ref (renconfig);
	renwindow->window = g_object_ref (gdkwindow);

	return renwindow;
}

static void
gdk_ren_window_debug_swap_buffers (GdkRenDrawable *drawable)
{
	g_return_if_fail (GDK_REN_IS_WINDOW_DEBUG (drawable));
}
