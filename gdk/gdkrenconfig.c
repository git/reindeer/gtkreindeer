/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gdkrenconfig.h"

#include "gdkrenbackend.h"

G_DEFINE_ABSTRACT_TYPE (GdkRenConfig, gdk_ren_config, G_TYPE_OBJECT)
static void
gdk_ren_config_finalize (GObject *gobject);

static void
gdk_ren_config_init (GdkRenConfig *self)
{

}

static void
gdk_ren_config_finalize (GObject *gobject)
{
	GdkRenConfig *self = GDK_REN_CONFIG (gobject);
	g_object_unref (self->screen);
	g_free ((GdkRenConfigAttr *) self->attributes);
	G_OBJECT_CLASS (gdk_ren_config_parent_class)->finalize (gobject);
}

static void
gdk_ren_config_class_init (GdkRenConfigClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	gobject_class->finalize = gdk_ren_config_finalize;
}

GdkRenBackend*
gdk_ren_config_get_backend (GdkRenConfig *config)
{
	g_return_val_if_fail (GDK_REN_IS_CONFIG (config), NULL);
	return config->backend;
}

GdkScreen*
gdk_ren_config_get_screen (GdkRenConfig *config)
{
	g_return_val_if_fail (GDK_REN_IS_CONFIG (config), NULL);
	return config->screen;
}

const GdkRenConfigAttr*
gdk_ren_config_get_attributes (GdkRenConfig *config)
{
	g_return_val_if_fail (GDK_REN_IS_CONFIG (config), NULL);
	return config->attributes;
}

const GdkRenConfigAttr GDK_REN_CONFIG_ATTR_DEFAULT =
{
	GDK_REN_CONFIG_CAVEAT_ANY, /* caveat */
	GDK_REN_RENDER_TYPE_ANY, /* render_type */
	GDK_REN_DRAWABLE_TYPE_WINDOW, /* drawable_type */
	GDK_REN_VISUAL_TYPE_ANY, /* visual_type */
	0, /* level */

	0, 0, 0, 0, 0, 0, 0, /* R G B A RA GA BA bits */
	0, /* depth_bits */
	0, /* stencil_bits */
	0, 0, 0, 0, 0, 0, 0, /* Accum : R G B A RA GA BA bits */

	-1, /* double_buffer */
	-1, /* stereo_buffer */
	0, /* aux_buffers */
	0, /* sample_buffers */
	0, /* samples */

	GDK_REN_TRANSPARENT_TYPE_DONT_CARE, /* transparent_type */
	-1, -1, -1, -1, /* transparent_{index,red,green,blue}_value */
};

gboolean
gdk_ren_config_attr_compare_default (
	const GdkRenConfigAttr *prev, const GdkRenConfigAttr *next)
{
	gint ret;

	ret = gdk_ren_config_attr_prefer_less_caveats (prev, next);
	if (ret >= 0) return ret;

	ret = gdk_ren_config_attr_prefer_wo_caveats (prev, next,
		GDK_REN_CONFIG_CAVEAT_NON_CONFORMANT);
	if (ret >= 0) return ret;

	ret = gdk_ren_config_attr_prefer_wo_caveats (prev, next,
		GDK_REN_CONFIG_CAVEAT_SLOW);
	if (ret >= 0) return ret;

	ret = gdk_ren_config_attr_prefer_visual_type (prev, next,
		GDK_REN_VISUAL_TYPE_TRUE_COLOR);
	if (ret >= 0) return ret;

	ret = gdk_ren_config_attr_prefer_true (
		prev->double_buffer, next->double_buffer);
	if (ret >= 0) return ret;

	/* Check all or most bits attributes here. Perhaps put them in other
	functions so applications can easily use them as well. */

	ret = gdk_ren_config_attr_prefer_more (
		prev->depth_bits, next->depth_bits);
	if (ret >= 0) return ret;

	ret = gdk_ren_config_attr_prefer_more (
		prev->stencil_bits, next->stencil_bits);
	if (ret >= 0) return ret;

	ret = gdk_ren_config_attr_prefer_true (
		prev->stereo_buffer, next->stereo_buffer);
	if (ret >= 0) return ret;

	/*
	Actually neither value is more correct than the other to return here below.
	But as long as we check most attributes above there should be no problem
	in theory.
	*/
	return TRUE;
}

gboolean
gdk_ren_config_attr_match (const GdkRenConfigAttr *attr,
	const GdkRenConfigAttr *tmpl)
{
	#define CHECK_IS_MORE(member)\
		if (attr->member < tmpl->member) return FALSE
	#define CHECK_IS_EQUAL(member)\
		if (tmpl->member != -1 && attr->member != tmpl->member) return FALSE

	if ((attr->caveat & ~(tmpl->caveat)) != 0)
		return FALSE;
	/* TODO: render_type, drawable_type, visual_type */
	if (attr->level != tmpl->level)
		return FALSE;

	CHECK_IS_MORE (red_bits);
	CHECK_IS_MORE (green_bits);
	CHECK_IS_MORE (blue_bits);
	CHECK_IS_MORE (alpha_bits);
	CHECK_IS_MORE (alpha_red_bits);
	CHECK_IS_MORE (alpha_green_bits);
	CHECK_IS_MORE (alpha_blue_bits);
	CHECK_IS_MORE (depth_bits);
	CHECK_IS_MORE (stencil_bits);
	CHECK_IS_MORE (accum_red_bits);
	CHECK_IS_MORE (accum_green_bits);
	CHECK_IS_MORE (accum_blue_bits);
	CHECK_IS_MORE (accum_alpha_bits);
	CHECK_IS_MORE (accum_alpha_red_bits);
	CHECK_IS_MORE (accum_alpha_green_bits);
	CHECK_IS_MORE (accum_alpha_blue_bits);

	CHECK_IS_EQUAL (double_buffer);
	CHECK_IS_EQUAL (stereo_buffer);
	CHECK_IS_MORE (aux_buffers);
	CHECK_IS_MORE (sample_buffers);
	CHECK_IS_MORE (samples);

	/* TODO: transparent_type */
	CHECK_IS_EQUAL (transparent_index_value);
	CHECK_IS_EQUAL (transparent_red_value);
	CHECK_IS_EQUAL (transparent_green_value);
	CHECK_IS_EQUAL (transparent_blue_value);

	#undef CHECK_IS_MORE
	#undef CHECK_IS_EQUAL

	return TRUE;
}
