/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gdk/gdkrenversion.h>

const guint gdk_ren_major_version = GDK_REN_MAJOR_VERSION;
const guint gdk_ren_minor_version = GDK_REN_MINOR_VERSION;
const guint gdk_ren_micro_version = GDK_REN_MICRO_VERSION;
const gchar *gdk_ren_compat_version = GDK_REN_COMPAT_VERSION;
