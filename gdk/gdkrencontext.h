/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GDK_REN_CONTEXT_H
#define GDK_REN_CONTEXT_H

#include <gdk/gdkrentypes.h>

#include <glib-object.h>
#include <ren/types.h>

G_BEGIN_DECLS

#define GDK_REN_TYPE_CONTEXT\
	(gdk_ren_context_get_type ())
#define GDK_REN_CONTEXT(obj)\
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), GDK_REN_TYPE_CONTEXT, GdkRenContext))
#define GDK_REN_CONTEXT_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_CAST ((klass), GDK_REN_TYPE_CONTEXT, GdkRenContextClass))
#define GDK_REN_IS_CONTEXT(obj)\
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GDK_REN_TYPE_CONTEXT))
#define GDK_REN_IS_CONTEXT_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_TYPE ((klass), GDK_REN_TYPE_CONTEXT))
#define GDK_REN_CONTEXT_GET_CLASS(obj)\
	(G_TYPE_INSTANCE_GET_CLASS ((obj), GDK_REN_TYPE_CONTEXT, GdkRenContextClass))

typedef struct _GdkRenContextClass GdkRenContextClass;

struct _GdkRenContext
{
	GObject parent;

	/* Protected */
	GdkRenConfig *config;
	GdkRenContext *share;
	GdkRenRenderType render_type;
	GdkRenDrawable *draw;
	GdkRenDrawable *read;
	RenReindeer *r;
	gboolean ren_inited;
};

struct _GdkRenContextClass
{
	GObjectClass parent;

/*
	gboolean (* copy_state) (
		GdkRenContext *context, GdkRenContext *dst,
		GdkRenState state);
*/

	void (* set_draw) (GdkRenContext *context, GdkRenDrawable *draw);
	void (* set_read) (GdkRenContext *context, GdkRenDrawable *read);

	RenReindeer* (* ren_begin) (GdkRenContext *context);
	void (* ren_end) (GdkRenContext *context);

	gint (* get_swap_interval) (GdkRenContext *context);
	gboolean (* set_swap_interval) (GdkRenContext *context, gint interval);
};

GType
gdk_ren_context_get_type (void);

GdkRenContext*
gdk_ren_context_make (
	GdkRenConfig *config, GdkRenContext *share, GdkRenRenderType render_type,
	GdkRenDrawable *draw, GdkRenDrawable *read, GError **error);

/*
gboolean
gdk_ren_context_copy_state (
	GdkRenContext *context, GdkRenContext *dst,
	GdkRenState state);
*/

void
gdk_ren_context_set_draw (GdkRenContext *context, GdkRenDrawable *draw);
void
gdk_ren_context_set_read (GdkRenContext *context, GdkRenDrawable *read);

gboolean
gdk_ren_context_ren_init (GdkRenContext *context);

RenReindeer*
gdk_ren_context_ren_begin (GdkRenContext *context);

void
gdk_ren_context_ren_end (GdkRenContext *context);

RenReindeer*
gdk_ren_context_get_reindeer (GdkRenContext *context);

GdkRenConfig*
gdk_ren_context_get_config (GdkRenContext *context);

GdkRenContext*
gdk_ren_context_get_share (GdkRenContext *context);

gint
gdk_ren_context_get_swap_interval (GdkRenContext *context);

gboolean
gdk_ren_context_set_swap_interval (GdkRenContext *context, gint interval);

G_END_DECLS

#endif
