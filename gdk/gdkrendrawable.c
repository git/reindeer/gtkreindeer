/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gdkrendrawable.h"

G_DEFINE_ABSTRACT_TYPE (GdkRenDrawable, gdk_ren_drawable, GDK_TYPE_DRAWABLE)

static void
gdk_ren_drawable_init (GdkRenDrawable *self)
{
}

static void
gdk_ren_drawable_class_init (GdkRenDrawableClass *klass)
{
}

void
gdk_ren_drawable_swap_buffers (GdkRenDrawable *drawable)
{
	return GDK_REN_DRAWABLE_GET_CLASS (drawable)->swap_buffers (drawable);
}
