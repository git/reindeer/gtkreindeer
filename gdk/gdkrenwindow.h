/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GDK_REN_WINDOW_H
#define GDK_REN_WINDOW_H

#include <gdk/gdkrendrawable.h>
#include <gdk/gdkrenconfig.h>

#include <glib-object.h>

G_BEGIN_DECLS

#define GDK_REN_TYPE_WINDOW\
	(gdk_ren_window_get_type ())
#define GDK_REN_WINDOW(obj)\
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), GDK_REN_TYPE_WINDOW, GdkRenWindow))
#define GDK_REN_WINDOW_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_CAST ((klass), GDK_REN_TYPE_WINDOW, GdkRenWindowClass))
#define GDK_REN_IS_WINDOW(obj)\
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GDK_REN_TYPE_WINDOW))
#define GDK_REN_IS_WINDOW_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_TYPE ((klass), GDK_REN_TYPE_WINDOW))
#define GDK_REN_WINDOW_GET_CLASS(obj)\
	(G_TYPE_INSTANCE_GET_CLASS ((obj), GDK_REN_TYPE_WINDOW, GdkRenWindowClass))

typedef struct _GdkRenWindowClass GdkRenWindowClass;

struct _GdkRenWindow
{
	GdkRenDrawable parent;

	GdkRenConfig *config;
	GdkWindow *window;
};

struct _GdkRenWindowClass
{
	GdkRenDrawableClass parent;
};

GType
gdk_ren_window_get_type (void);

GdkRenWindow*
gdk_ren_window_make (GdkRenConfig *config, GdkWindow *window, GError **error);

G_END_DECLS

#endif
