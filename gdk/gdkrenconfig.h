/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GDK_REN_CONFIG_H
#define GDK_REN_CONFIG_H

#include <gdk/gdkrentypes.h>

#include <glib-object.h>
#include <gdk/gdk.h>

G_BEGIN_DECLS

#define GDK_REN_TYPE_CONFIG\
	(gdk_ren_config_get_type ())
#define GDK_REN_CONFIG(obj)\
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), GDK_REN_TYPE_CONFIG, GdkRenConfig))
#define GDK_REN_CONFIG_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_CAST ((klass), GDK_REN_TYPE_CONFIG, GdkRenConfigClass))
#define GDK_REN_IS_CONFIG(obj)\
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GDK_REN_TYPE_CONFIG))
#define GDK_REN_IS_CONFIG_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_TYPE ((klass), GDK_REN_TYPE_CONFIG))
#define GDK_REN_CONFIG_GET_CLASS(obj)\
	(G_TYPE_INSTANCE_GET_CLASS ((obj), GDK_REN_TYPE_CONFIG, GdkRenConfigClass))

typedef struct _GdkRenConfigClass GdkRenConfigClass;

struct _GdkRenConfig
{
	GObject parent;

	/* Protected */
	GdkRenBackend *backend;
	GdkScreen *screen;
	GdkRenConfigAttr *attributes;
};

struct _GdkRenConfigClass
{
	GObjectClass parent;
};

struct _GdkRenConfigAttr
{
	/*GdkRenConfigCaveat*/ guint32 caveat;
	/*GdkRenRenderType*/ guint32 render_type;
	/*GdkRenDrawableType*/ guint32 drawable_type;
	/*GdkRenVisualType*/ guint32 visual_type;
	gint32 level;

	guint8 red_bits;
	guint8 green_bits;
	guint8 blue_bits;
	guint8 alpha_bits;
	guint8 alpha_red_bits;
	guint8 alpha_green_bits;
	guint8 alpha_blue_bits;
	guint8 depth_bits;
	guint8 stencil_bits;
	guint8 accum_red_bits;
	guint8 accum_green_bits;
	guint8 accum_blue_bits;
	guint8 accum_alpha_bits;
	guint8 accum_alpha_red_bits;
	guint8 accum_alpha_green_bits;
	guint8 accum_alpha_blue_bits;

	gint8 double_buffer;
	gint8 stereo_buffer;
	guint16 aux_buffers;
	guint16 sample_buffers;
	guint16 samples;

	enum
	{
		GDK_REN_TRANSPARENT_TYPE_ALPHA = 0,
		GDK_REN_TRANSPARENT_TYPE_RGB = (1 << 0),
		GDK_REN_TRANSPARENT_TYPE_INDEX = (1 << 1),
		GDK_REN_TRANSPARENT_TYPE_DONT_CARE = ~0,
	} transparent_type;
	gint16 transparent_index_value;
	gint16 transparent_red_value;
	gint16 transparent_green_value;
	gint16 transparent_blue_value;
};

GType
gdk_ren_config_get_type (void);

GdkRenConfig*
gdk_ren_config_find (GdkRenBackend *backend, GdkScreen *screen,
	const GdkRenConfigAttr *attributes, GdkRenConfigAttrCompareFunc compare,
	GError **error);

GdkRenBackend*
gdk_ren_config_get_backend (GdkRenConfig *config);

GdkScreen*
gdk_ren_config_get_screen (GdkRenConfig *config);

const GdkRenConfigAttr*
gdk_ren_config_get_attributes (GdkRenConfig *config);

extern const GdkRenConfigAttr GDK_REN_CONFIG_ATTR_DEFAULT;

gboolean
gdk_ren_config_attr_compare_default (
	const GdkRenConfigAttr *prev, const GdkRenConfigAttr *next);

/* This function is mainly used by backend config implementations. */
gboolean
gdk_ren_config_attr_match (const GdkRenConfigAttr *attr,
	const GdkRenConfigAttr *tmpl);

static inline guint
gdk_ren_uint32_count_bits_set (guint32 v)
{
	/*
	Source:
	http://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetParallel
	*/
	v = v - ((v >> 1) & 0x55555555);
	v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
	return (((v + (v >> 4)) & 0xF0F0F0F) * 0x1010101) >> 24;
}

/*
The prefer functions below are used to make it easier to implement compare
functions. They return TRUE and FALSE if that should be returned, -1
if both prev and next are preferable, and -2 if neither are preferable.
*/

static inline gint
gdk_ren_config_attr_prefer_wo_caveats (
	const GdkRenConfigAttr *prev, const GdkRenConfigAttr *next,
	GdkRenConfigCaveat caveats)
{
	GdkRenConfigCaveat pc = prev->caveat & caveats;
	GdkRenConfigCaveat nc = next->caveat & caveats;
	guint npc = gdk_ren_uint32_count_bits_set (pc);
	guint nnc = gdk_ren_uint32_count_bits_set (nc);
	if (npc != nnc)
		return (nnc < npc);
	else
		return (nnc == 0) ? -1 : -2;
}

static inline gint
gdk_ren_config_attr_prefer_less_caveats (
	const GdkRenConfigAttr *prev, const GdkRenConfigAttr *next)
{
	guint npc = gdk_ren_uint32_count_bits_set (prev->caveat);
	guint nnc = gdk_ren_uint32_count_bits_set (next->caveat);
	return (npc == nnc) ? -1 : (nnc < npc);
}

static inline gint
gdk_ren_config_attr_prefer_visual_type (
	const GdkRenConfigAttr *prev, const GdkRenConfigAttr *next,
	GdkRenVisualType visual_type)
{
	gboolean prev_right = (prev->visual_type == visual_type);
	gboolean next_right = (next->visual_type == visual_type);
	if (prev_right != next_right)
		return next_right;
	else
		return next_right ? -1 : -2;
}

static inline gint
gdk_ren_config_attr_prefer_more (guint prev, guint next)
{
	return (prev == next) ? -1 : (next > prev);
}

static inline gint
gdk_ren_config_attr_prefer_less (guint prev, guint next)
{
	return (prev == next) ? -1 : (next < prev);
}

static inline gint
gdk_ren_config_attr_prefer_more_and_close (guint prev, guint next, guint target)
{
	if (prev == target && next == target)
		return -1;
	gboolean prev_more = (prev > target);
	gboolean next_more = (next > target);
	if (prev_more != next_more)
		return next_more;
	return (prev == next) ? -1 : (next < prev);
}

static inline gint
gdk_ren_config_attr_prefer_true (gboolean prev, gboolean next)
{
	if (prev == next)
		return next != FALSE ? -1 : -2;
	else
		return next != FALSE;
}

static inline gint
gdk_ren_config_attr_prefer_false (gboolean prev, gboolean next)
{
	if (prev == next)
		return next == FALSE ? -1 : -2;
	else
		return next == FALSE;
}

G_END_DECLS

#endif
