/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gdkrenwindow.h"

#include "gdkrenbackend.h"

G_DEFINE_ABSTRACT_TYPE (GdkRenWindow, gdk_ren_window, GDK_REN_TYPE_DRAWABLE)
static void
gdk_ren_window_finalize (GObject *gobject);

static void
gdk_ren_window_init (GdkRenWindow *self)
{
}

static void
gdk_ren_window_finalize (GObject *gobject)
{
	GdkRenWindow *self = GDK_REN_WINDOW (gobject);
	g_object_unref (self->config);
	g_object_unref (self->window);
	G_OBJECT_CLASS (gdk_ren_window_parent_class)->finalize (gobject);
}

static void
gdk_ren_window_class_init (GdkRenWindowClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	gobject_class->finalize = gdk_ren_window_finalize;
}
