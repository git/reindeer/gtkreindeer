/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gdkrencontext.h"

#include "gdkrenbackend.h"
#include "gdkrenconfig.h"

G_DEFINE_ABSTRACT_TYPE (GdkRenContext, gdk_ren_context, G_TYPE_OBJECT)
static void
gdk_ren_context_finalize (GObject *gobject);

static void
gdk_ren_context_init (GdkRenContext *self)
{
}

static void
gdk_ren_context_finalize (GObject *gobject)
{
	GdkRenContext *self = GDK_REN_CONTEXT (gobject);
	g_object_unref (self->config);
	if (self->share != NULL)
		g_object_unref (self->share);
	if (self->draw != NULL)
		g_object_unref (self->draw);
	if (self->read != NULL)
		g_object_unref (self->read);
	ren_reindeer_unref (self->r);
	G_OBJECT_CLASS (gdk_ren_context_parent_class)->finalize (gobject);
}

static void
gdk_ren_context_class_init (GdkRenContextClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	gobject_class->finalize = gdk_ren_context_finalize;
}

/*
gboolean
gdk_ren_context_copy_state (
	GdkRenContext *context, GdkRenContext *dst,
	GdkRenState state)
{
	return GDK_REN_CONTEXT_GET_CLASS (context)->
		copy_state (context, dst, state);
}
*/

void
gdk_ren_context_set_draw (GdkRenContext *context, GdkRenDrawable *draw)
{
	return GDK_REN_CONTEXT_GET_CLASS (context)->set_draw (context, draw);
}
void
gdk_ren_context_set_read (GdkRenContext *context, GdkRenDrawable *read)
{
	return GDK_REN_CONTEXT_GET_CLASS (context)->set_read (context, read);
}

gboolean
gdk_ren_context_ren_init (GdkRenContext *context)
{
	g_return_val_if_fail (GDK_REN_IS_CONTEXT (context), FALSE);
	if (!context->ren_inited)
	{
		gdk_ren_context_ren_begin (context);
		context->ren_inited = ren_init (context->r);
		gdk_ren_context_ren_end (context);
	}
	return context->ren_inited;
}

RenReindeer*
gdk_ren_context_ren_begin (GdkRenContext *context)
{
	return GDK_REN_CONTEXT_GET_CLASS (context)->ren_begin (context);
}

void
gdk_ren_context_ren_end (GdkRenContext *context)
{
	return GDK_REN_CONTEXT_GET_CLASS (context)->ren_end (context);
}

RenReindeer*
gdk_ren_context_get_reindeer (GdkRenContext *context)
{
	g_return_val_if_fail (GDK_REN_IS_CONTEXT (context), NULL);
	return context->r;
}

GdkRenConfig*
gdk_ren_context_get_config (GdkRenContext *context)
{
	g_return_val_if_fail (GDK_REN_IS_CONTEXT (context), NULL);
	return context->config;
}

GdkRenContext*
gdk_ren_context_get_share (GdkRenContext *context)
{
	g_return_val_if_fail (GDK_REN_IS_CONTEXT (context), NULL);
	return context->share;
}

gint
gdk_ren_context_get_swap_interval (GdkRenContext *context)
{
	g_return_val_if_fail (GDK_REN_IS_CONTEXT (context), -1);
	return GDK_REN_CONTEXT_GET_CLASS (context)->get_swap_interval (context);
}

gboolean
gdk_ren_context_set_swap_interval (GdkRenContext *context, gint interval)
{
	g_return_val_if_fail (GDK_REN_IS_CONTEXT (context), FALSE);
	return GDK_REN_CONTEXT_GET_CLASS (context)->set_swap_interval (context, interval);
}
