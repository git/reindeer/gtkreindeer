/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GDK_REN_TYPES_H
#define GDK_REN_TYPES_H

#include <glib.h>

typedef struct _GdkRenBackend GdkRenBackend;
typedef struct _GdkRenConfig GdkRenConfig;
typedef struct _GdkRenContext GdkRenContext;

typedef struct _GdkRenDrawable GdkRenDrawable;
typedef struct _GdkRenWindow GdkRenWindow;
typedef struct _GdkRenPixmap GdkRenPixmap;

#define GDK_REN_ERROR gdk_ren_error_quark ()
GQuark
gdk_ren_error_quark (void);

typedef enum
{
	GDK_REN_ERROR_FAILED,
	GDK_REN_ERROR_NO_CONFIG_MATCH,
	GDK_REN_ERROR_RENDER_TYPE_UNSUPPORTED,
	GDK_REN_ERROR_WINDOWS_UNSUPPORTED,
} GdkRenError;

typedef struct _GdkRenConfigAttr GdkRenConfigAttr;
typedef gboolean (* GdkRenConfigAttrCompareFunc) (
	const GdkRenConfigAttr *prev, const GdkRenConfigAttr *next);

typedef enum
{
	GDK_REN_CONFIG_CAVEAT_NONE = 0,
	GDK_REN_CONFIG_CAVEAT_SLOW = (1 << 0),
	GDK_REN_CONFIG_CAVEAT_NON_CONFORMANT = (1 << 1),
	GDK_REN_CONFIG_CAVEAT_ANY = ~0,
} GdkRenConfigCaveat;

typedef enum
{
	GDK_REN_RENDER_TYPE_RGBA = (1 << 0),
	GDK_REN_RENDER_TYPE_INDEX = (1 << 1),
	GDK_REN_RENDER_TYPE_RAGABA = (1 << 2),
	GDK_REN_RENDER_TYPE_ANY = ~0,
} GdkRenRenderType;

typedef enum
{
	GDK_REN_DRAWABLE_TYPE_WINDOW = (1 << 0),
	GDK_REN_DRAWABLE_TYPE_PIXMAP = (1 << 1),
	GDK_REN_DRAWABLE_TYPE_PBUFFER = (1 << 2),
	GDK_REN_DRAWABLE_TYPE_MEMORY = (1 << 3),
	GDK_REN_DRAWABLE_TYPE_ANY = ~0,
} GdkRenDrawableType;

typedef enum
{
	GDK_REN_VISUAL_TYPE_STATIC_GRAY = (1 << 0),
	GDK_REN_VISUAL_TYPE_GRAYSCALE = (1 << 1),
	GDK_REN_VISUAL_TYPE_STATIC_COLOR = (1 << 2),
	GDK_REN_VISUAL_TYPE_PSEUDO_COLOR = (1 << 3),
	GDK_REN_VISUAL_TYPE_TRUE_COLOR = (1 << 4),
	GDK_REN_VISUAL_TYPE_DIRECT_COLOR = (1 << 5),
	GDK_REN_VISUAL_TYPE_ANY = ~0,
} GdkRenVisualType;

typedef enum
{
	GDK_REN_STATE_VXBUFFERS = (1 << 0),
	GDK_REN_STATE_TEXTURES = (1 << 1),

	GDK_REN_STATE_ALL = ~0,
} GdkRenState;

#endif /* GDK_REN_TYPES_H */
