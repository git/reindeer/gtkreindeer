/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GDK_REN_BACKEND_H
#define GDK_REN_BACKEND_H

#include <gdk/gdkrentypes.h>
#include <glib.h>
#include <ren/ren.h>

G_BEGIN_DECLS

gboolean
gdk_ren_init (GError **error);

void
gdk_ren_exit (void);

GdkRenBackend*
gdk_ren_backend_lookup (const gchar *name, GError **error);

gboolean
gdk_ren_backend_use (GdkRenBackend *backend, GError **error);

void
gdk_ren_backend_unuse (GdkRenBackend *backend);

RenBackend*
gdk_ren_backend_get_ren_backend (GdkRenBackend *backend);

/*
guint
gdk_ren_backend_contexts_per_drawable (GdkRenBackend* backend);
guint
gdk_ren_backend_drawables_per_context (GdkRenBackend* backend);
gboolean
gdk_ren_backend_many_contexts (GdkRenBackend *backend);
gboolean
gdk_ren_backend_context_share_possible (GdkRenBackend* backend);
gboolean
gdk_ren_backend_context_separate_read (GdkRenBackend* backend);
gboolean
gdk_ren_backend_support_overlay (GdkRenBackend* backend);
gboolean
gdk_ren_backend_support_underlay (GdkRenBackend* backend);
*/

G_END_DECLS

#endif
