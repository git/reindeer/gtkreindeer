/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#if HAVE_CONFIG_H
#include "config.h"
#endif
#include "gdkrenbackend.h"

#include <glib-object.h>
#include <gdk/gdk.h>
#include <gdk/gdkrenconfig.h>
#include <gdk/gdkrencontext.h>
#include <ren/ren.h>
#include <ltdl.h>
#include <string.h>
#include "filevercmp.h"

static gboolean initialized = FALSE;
static gboolean exited = FALSE;
static const gchar *target = AC_gdktarget;
static gchar **search_paths = NULL;
static GHashTable *backend_table; /* RenBackend* to GdkRenBackend* */

#define GDK_REN_TYPE_MODULE\
	(gdk_ren_module_get_type ())
#define GDK_REN_MODULE(obj)\
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), GDK_REN_TYPE_MODULE, GdkRenModule))
#define GDK_REN_MODULE_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_CAST ((klass), GDK_REN_TYPE_MODULE, GdkRenModuleClass))
#define GDK_REN_IS_MODULE(obj)\
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GDK_REN_TYPE_MODULE))
#define GDK_REN_IS_MODULE_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_TYPE ((klass), GDK_REN_TYPE_MODULE))
#define GDK_REN_MODULE_GET_CLASS(obj)\
	(G_TYPE_INSTANCE_GET_CLASS ((obj), GDK_REN_TYPE_MODULE, GdkRenModuleClass))

typedef struct _GdkRenModule GdkRenModule;
typedef struct _GdkRenModuleClass GdkRenModuleClass;

struct _GdkRenModule
{
	GTypeModule parent;

	gchar *name;
	gchar *filename;

	GError **error;

	lt_dlhandle libhandle;

	GdkRenConfig* (* config_find) (GdkRenBackend *backend,
		GdkScreen *screen, const GdkRenConfigAttr *attributes,
		GdkRenConfigAttrCompareFunc compare, GError **error);

	GdkRenContext* (* context_make) (GdkRenConfig *config,
		GdkRenContext *share, GdkRenRenderType render_type,
		GdkRenDrawable *draw, GdkRenDrawable *read, GError **error);

	GdkRenWindow* (* window_make) (GdkRenConfig *config, GdkWindow *window,
		GError **error);
};

struct _GdkRenModuleClass
{
	GTypeModuleClass parent;
};

static GType
gdk_ren_module_get_type (void);

static gboolean
gdk_ren_module_load (GTypeModule *module);

static void
gdk_ren_module_unload (GTypeModule *module);

struct _GdkRenBackend
{
	guint use_count;
	gchar *name;
	gchar *nicename;
	gchar *desc;
	gchar *version;
	RenBackend *ren_backend;
	GdkRenModule *module;
};

static void
gdk_ren_backend_destroy (GdkRenBackend* backend);

GQuark
gdk_ren_error_quark (void)
{
  return g_quark_from_static_string ("gdk-ren-error-quark");
}

gboolean
gdk_ren_init (GError **error)
{
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	if (initialized)
		return TRUE;
	if (exited)
		return FALSE;

	if (!ren_library_is_inited ())
	{
		g_warning ("Trying to initialize GDK-Reindeer without Reindeer being initialized!");
		return FALSE;
	}
	if (lt_dlinit () != 0)
	{
		g_set_error_literal (error, GDK_REN_ERROR, GDK_REN_ERROR_FAILED, lt_dlerror ());
		return FALSE;
	}

	const gchar *gdkreindeer_path = g_getenv ("GDK_REINDEER_PATH");
	if (gdkreindeer_path != NULL)
	{
		static const gchar separator[] = {LT_PATHSEP_CHAR, '\0'};
		search_paths = g_strsplit (gdkreindeer_path, separator, 0);
	}
	if (search_paths == NULL)
	{
		search_paths = g_new (gchar*, 2);
		search_paths[0] = g_strdup (AC_gdkrenlibdir);
		search_paths[1] = NULL;
	}

	backend_table = g_hash_table_new_full (
		g_direct_hash, g_direct_equal,
		NULL, (GDestroyNotify) gdk_ren_backend_destroy);

	initialized = TRUE;
	return TRUE;
}

void
gdk_ren_exit (void)
{
	if (!initialized || exited)
		return;

	g_strfreev (search_paths);

	g_hash_table_destroy (backend_table);
	if (lt_dlexit () != 0)
	{
		g_message ("LTDL exit error on GDK-Reindeer library exit: %s",
			lt_dlerror ());
	}

	initialized = FALSE;
	exited = TRUE;
}

G_DEFINE_TYPE (GdkRenModule, gdk_ren_module, G_TYPE_TYPE_MODULE)

static void
gdk_ren_module_init (GdkRenModule *self)
{
	self->error = NULL;
}

static void
gdk_ren_module_class_init (GdkRenModuleClass *klass)
{
	GTypeModuleClass *tm_class = (GTypeModuleClass *) klass;
	tm_class->load = gdk_ren_module_load;
	tm_class->unload = gdk_ren_module_unload;
}

static gint
lib_compare (gconstpointer a, gconstpointer b, gpointer user_data)
{
	const char *name_a = g_strrstr (a, "/");
	if (name_a == NULL)
		name_a = a;
	const char *name_b = g_strrstr (b, "/");
	if (name_b == NULL)
		name_b = b;
	return -filevercmp (name_a, name_b);
}

static inline void*
module_symbol (lt_dlhandle libhandle,
	const char *module_name, const char *name)
{
	char *symbol_name;
	if (strcmp(module_name, "debug") != 0)
	{
		symbol_name = g_strdup_printf ("gdk_ren__%s_%s__%s",
			module_name, target, name);
	}
	else
	{
		symbol_name = g_strdup_printf ("gdk_ren__%s__%s",
			module_name, name);
	}
	void *symbol = lt_dlsym (libhandle, symbol_name);
	g_free (symbol_name);
	return symbol;
}

static inline void*
module_type_symbol (lt_dlhandle libhandle,
	const char *module_name, const char* type, const char *name)
{
	char *symbol_name;
	if (strcmp(module_name, "debug") != 0)
	{
		symbol_name = g_strdup_printf ("gdk_ren_%s_%s_%s_%s",
			type, module_name, target, name);
	}
	else
	{
		symbol_name = g_strdup_printf ("gdk_ren_%s_%s_%s",
			type, module_name, name);
	}
	void *symbol = lt_dlsym (libhandle, symbol_name);
	g_free (symbol_name);
	return symbol;
}

static gboolean
gdk_ren_module_load (GTypeModule *type_module)
{
	GdkRenModule *module = GDK_REN_MODULE (type_module);
	GError **error = module->error;

	char *libname;
	if (strcmp(module->name, "debug") != 0)
		libname = g_strconcat ("libgdkreindeer-", target, "-", module->name, ".", NULL);
	else
		libname = g_strconcat ("libgdkreindeer-", module->name, ".", NULL);

	GSequence *files = g_sequence_new (g_free);

	const gchar *path;
	gint i = 0;
	for (path = search_paths[i]; path != NULL; path = search_paths[++i])
	{
		GDir *dir = g_dir_open (path, 0, NULL);
		if (dir == NULL)
			continue;
		const gchar *filename;
		while ((filename = g_dir_read_name (dir)) != NULL)
		{
			if (g_str_has_prefix (filename, libname) &&
				g_str_has_suffix (filename, LT_MODULE_EXT))
			{
				gchar *file = g_strdup_printf ("%s/%s",
					path, filename);
				g_sequence_insert_sorted (files, file, lib_compare, NULL);
			}
		}
		g_dir_close (dir);
	}

	if (g_sequence_get_length (files) == 0)
		goto FAIL;

	GSequenceIter *iter = g_sequence_get_begin_iter (files);
	while (!g_sequence_iter_is_end (iter))
	{
		const gchar *filename = g_sequence_get (iter);

		lt_dlhandle libhandle = lt_dlopenext (filename);
		if (libhandle == NULL)
		{
			g_message ("Could not load library `%s': %s",
				filename, lt_dlerror ());
			goto BAD_FILE;
		}

		const gchar **compat = module_symbol (libhandle,
			module->name, "compat_version");
		if (compat == NULL)
		{
			g_message ("Library `%s' is missing compatibility information",
				filename);
			goto BAD_FILE;
		}
		if (strcmp (*compat, ren_compat_version) != 0)
		{
			g_message ("Library `%s' is not compatible (%s instead of %s)",
				filename, *compat, ren_compat_version);
			goto BAD_FILE;
		}

		void (* register_type) (GTypeModule *module);

		module->config_find = module_type_symbol (libhandle,
			module->name, "config", "find");
		register_type = module_type_symbol (libhandle,
			module->name, "config", "type_register");
		if (module->config_find == NULL || register_type == NULL)
		{
			g_message ("GDK-Reindeer module `%s' is missing GdkRenConfig implementation",
				module->name);
			goto BAD_FILE;
		}
		register_type (type_module);

		module->context_make = module_type_symbol (libhandle,
			module->name, "context", "make");
		register_type = module_type_symbol (libhandle,
			module->name, "context", "type_register");
		if (module->context_make == NULL || register_type == NULL)
		{
			g_message ("GDK-Reindeer module `%s' is missing GdkRenContext implementation",
				module->name);
			goto BAD_FILE;
		}
		register_type (type_module);

		module->window_make = module_type_symbol (libhandle,
			module->name, "window", "make");
		register_type = module_type_symbol (libhandle,
			module->name, "window", "type_register");
		if (module->window_make != NULL && register_type != NULL)
			register_type (type_module);
		else
			module->window_make = NULL;

		module->libhandle = libhandle;
		module->filename = g_strdup (filename);
		break;

		BAD_FILE:
		if (libhandle) lt_dlclose (libhandle);
		module->config_find = NULL;
		module->context_make = NULL;
		module->window_make = NULL;
		iter = g_sequence_iter_next (iter);
		continue;
	}
	if (g_sequence_iter_is_end (iter))
		goto FAIL;

	g_free (libname);
	g_sequence_free (files);
	return TRUE;

	FAIL:
	g_free (libname);
	g_sequence_free (files);
	g_set_error (error, GDK_REN_ERROR, GDK_REN_ERROR_FAILED,
		"Could not load GDK-Reindeer module `%s'", module->name);
	return FALSE;
}

static void
gdk_ren_module_unload (GTypeModule *type_module)
{
	GdkRenModule *module = GDK_REN_MODULE (type_module);

	if (lt_dlclose (module->libhandle) != 0)
	{
		g_warning ("%s", lt_dlerror ());
	}
	else
	{
		module->libhandle = NULL;
		module->config_find = NULL;
		module->context_make = NULL;
		module->window_make = NULL;
	}
}

GdkRenBackend*
gdk_ren_backend_lookup (const gchar *name, GError **error)
{
	g_return_val_if_fail (error == NULL || *error == NULL, NULL);

	GdkRenBackend *backend;
	RenBackend *ren_backend;

	if (!initialized)
	{
		g_critical ("GDK-Reindeer library is not initialized");
		return NULL;
	}

	RenError *renerror = NULL;
	ren_backend = ren_backend_lookup (name, &renerror);
	if (ren_backend == NULL)
	{
		g_set_error_literal (error, GDK_REN_ERROR, GDK_REN_ERROR_FAILED,
			renerror->message);
		ren_error_free (renerror);
		return NULL;
	}

	backend = g_hash_table_lookup (backend_table, ren_backend);
	if (backend == NULL)
	{
		GdkRenModule *module = g_object_new (GDK_REN_TYPE_MODULE, NULL);
		module->name = g_strdup (name);
		backend = g_new0 (GdkRenBackend, 1);
		backend->name = g_strdup (name);
		backend->ren_backend = ren_backend;
		backend->module = module;

		g_hash_table_insert (backend_table, ren_backend, backend);

		if (!gdk_ren_backend_use (backend, error))
			backend = NULL;
		ren_backend_unuse (ren_backend);
	}

	return backend;
}

gboolean
gdk_ren_backend_use (GdkRenBackend *backend, GError **error)
{
	g_return_val_if_fail (backend != NULL, FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	if (++(backend->use_count) != 1)
		return TRUE;

	GdkRenModule *module = backend->module;
	module->error = error;
	if (!g_type_module_use ((GTypeModule *) module))
		return FALSE;
	RenError *renerror = NULL;
	if (!ren_backend_use (backend->ren_backend, &renerror))
	{
		g_type_module_unuse ((GTypeModule *) module);
		g_set_error_literal (error, GDK_REN_ERROR, GDK_REN_ERROR_FAILED,
			renerror->message);
		ren_error_free (renerror);
		return FALSE;
	}
	return TRUE;
}

void
gdk_ren_backend_unuse (GdkRenBackend *backend)
{
	g_return_if_fail (backend != NULL);

	if (--(backend->use_count) > 0)
		return;

	GdkRenModule *module = backend->module;
	g_type_module_unuse ((GTypeModule *) module);
	ren_backend_unuse (backend->ren_backend);
}

GSList* /* of GdkRenBackend* */
gdk_ren_backend_list (void)
{
	/* TODO */
	return NULL;
}

RenBackend*
gdk_ren_backend_get_ren_backend (GdkRenBackend *backend)
{
	return backend->ren_backend;
}

static void
gdk_ren_backend_destroy (GdkRenBackend* backend)
{
	//g_object_unref (backend->module); /* Do not unref this! */
	g_free (backend->name);
	g_free (backend);
}

GdkRenConfig*
gdk_ren_config_find (GdkRenBackend *backend, GdkScreen *screen,
	const GdkRenConfigAttr *attributes, GdkRenConfigAttrCompareFunc compare,
	GError **error)
{
	g_return_val_if_fail (backend != NULL, NULL);
	g_return_val_if_fail (error == NULL || *error == NULL, NULL);

	GdkRenModule *module = backend->module;
	GdkRenConfig *config = NULL;

	module->error = error;
	if (!g_type_module_use ((GTypeModule *) module))
		goto FAIL;
	config = module->config_find (backend, screen, attributes, compare, error);
	g_type_module_unuse ((GTypeModule *) module);
	if (config == NULL)
		goto FAIL;

	return config;

	FAIL:
	if (error != NULL && *error != NULL && (*error)->message == NULL)
	{
		if ((*error)->code == GDK_REN_ERROR_NO_CONFIG_MATCH)
			(*error)->message = g_strdup ("No matching configuration");
	}
	g_prefix_error (error, "Could not find GDK-Reindeer config: ");
	return NULL;
}

GdkRenContext*
gdk_ren_context_make (
	GdkRenConfig *config, GdkRenContext *share, GdkRenRenderType render_type,
	GdkRenDrawable *draw, GdkRenDrawable *read, GError **error)
{
	g_return_val_if_fail (GDK_REN_IS_CONFIG (config), NULL);
	g_return_val_if_fail (error == NULL || *error == NULL, NULL);

	GdkRenBackend *backend = gdk_ren_config_get_backend (config);
	GdkRenModule *module = backend->module;
	GdkRenContext *context = NULL;

	module->error = error;
	if (!g_type_module_use ((GTypeModule *) module))
		goto FAIL;
	context = module->context_make (config, share, render_type, draw, read, error);
	g_type_module_unuse ((GTypeModule *) module);
	if (context == NULL)
		goto FAIL;

	return context;

	FAIL:
	if (error != NULL && *error != NULL && (*error)->message == NULL)
	{
		if ((*error)->code == GDK_REN_ERROR_RENDER_TYPE_UNSUPPORTED)
			(*error)->message = g_strdup ("Render type not supported");
	}
	g_prefix_error (error, "Could not make GDK-Reindeer context: ");
	return NULL;
}

GdkRenWindow*
gdk_ren_window_make (GdkRenConfig *config, GdkWindow *gdk_window, GError **error)
{
	g_return_val_if_fail (GDK_REN_IS_CONFIG (config), NULL);
	g_return_val_if_fail (error == NULL || *error == NULL, NULL);

	GdkRenBackend *backend = gdk_ren_config_get_backend (config);
	GdkRenModule *module = backend->module;
	GdkRenWindow *window = NULL;

	module->error = error;
	if (!g_type_module_use ((GTypeModule *) module))
		goto FAIL;
	if (module->window_make != NULL)
	{
		window = module->window_make (config, gdk_window, error);
	}
	else
	{
		g_set_error (error, GDK_REN_ERROR, GDK_REN_ERROR_WINDOWS_UNSUPPORTED,
			"Backend does not support on-screen windows");
	}
	g_type_module_unuse ((GTypeModule *) module);
	if (window == NULL)
		goto FAIL;

	return window;

	FAIL:
	g_prefix_error (error, "Could not make GDK-Reindeer window: ");
	return NULL;
}
