/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

[CCode (cprefix = "GdkRen", lower_case_cprefix = "gdk_ren_", cheader_filename = "gdk/gdkren.h")]
namespace Gdk.Ren
{
	[CCode (cname = "gdk_ren_init")]
	public bool init () throws Error;
	[CCode (cname = "gdk_ren_exit")]
	public void exit ();

	[CCode (cname = "GdkRenBackend*", cprefix = "gdk_ren_backend_")]
	[SimpleType]
	public struct Backend
	{
		public static Backend lookup (string name) throws Error;
		public bool use () throws Error;
		public void unuse ();
		public Ren.Backend get_ren_backend ();
	}

	errordomain Error
	{
		FAILED,
		NO_CONFIG_MATCH,
		RENDER_TYPE_UNSUPPORTED,
		WINDOWS_UNSUPPORTED,
	}

	public enum ConfigCaveat
	{
		NONE, SLOW, NON_CONFORMANT, ANY
	}

	public enum RenderType
	{
		RGBA, INDEX, RAGABA, ANY
	}

	public enum DrawableType
	{
		WINDOW, PIXMAP, PBUFFER, MEMORY, ANY
	}

	public enum VisualType
	{
		STATIC_GRAY, GRAYSCALE,
		STATIC_COLOR, PSEUDO_COLOR, TRUE_COLOR, DIRECT_COLOR,
		ANY
	}

	[Immutable]
	public struct ConfigAttr
	{
		public const ConfigAttr DEFAULT;
		public static bool compare_default (ConfigAttr prev, ConfigAttr next);

		public uint32 caveat;
		public uint32 render_type;
		public uint32 drawable_type;
		public uint32 visual_type;
		public int32 level;

		public uint8 red_bits;
		public uint8 green_bits;
		public uint8 blue_bits;
		public uint8 alpha_bits;
		public uint8 alpha_red_bits;
		public uint8 alpha_green_bits;
		public uint8 alpha_blue_bits;
		public uint8 depth_bits;
		public uint8 stencil_bits;
		public uint8 accum_red_bits;
		public uint8 accum_green_bits;
		public uint8 accum_blue_bits;
		public uint8 accum_alpha_bits;
		public uint8 accum_alpha_red_bits;
		public uint8 accum_alpha_green_bits;
		public uint8 accum_alpha_blue_bits;

		public int8 double_buffer;
		public int8 stereo_buffer;
		public uint16 aux_buffers;
		public uint16 sample_buffers;
		public uint16 samples;

		public uint32 transparent_type;
		public int16 transparent_index_value;
		public int16 transparent_red_value;
		public int16 transparent_green_value;
		public int16 transparent_blue_value;

		public static int prefer_wo_caveats (ConfigAttr prev, ConfigAttr next, ConfigCaveat caveats);
		public static int prefer_less_caveats (ConfigAttr prev, ConfigAttr next);
		public static int prefer_visual_type (ConfigAttr prev, ConfigAttr next, VisualType visual_type);
		public static int prefer_more (uint prev, uint next);
		public static int prefer_less (uint prev, uint next);
		public static int prefer_more_and_close (uint prev, uint next, uint target);
		public static int prefer_true (bool prev, bool next);
		public static int prefer_false (bool prev, bool next);
	}
	[CCode (has_target = false)]
	public delegate bool ConfigAttrCompareFunc (ConfigAttr prev, ConfigAttr next);
	public uint uint32_count_bits_set (uint32 v);

	public class Config : GLib.Object
	{
		public static Config? find (Backend backend, Gdk.Screen? screen,
			ConfigAttr attributes, ConfigAttrCompareFunc compare) throws Error;
		public Backend get_backend ();
		public Gdk.Screen get_screen ();
		public unowned ConfigAttr* get_attributes ();
	}

	public class Context : GLib.Object
	{
		public static Context? make (Config config, Context share,
			RenderType render_type, Drawable draw, Drawable read) throws Error;
		public void set_draw (Drawable draw);
		public void set_read (Drawable read);
		public bool init ();
		public unowned global::Ren.Reindeer ren_begin ();
		public void ren_end ();
		public unowned global::Ren.Reindeer get_reindeer ();
		public Config get_config ();
		public Context get_share ();
		public int get_swap_interval ();
		public bool set_swap_interval (int interval);
	}

	public class Drawable : GLib.Object
	{
		public void swap_buffers ();
	}

	public class Window : Drawable
	{
		public static Window? make (Config config, Gdk.Window gdkwindow) throws Error;
	}
}
