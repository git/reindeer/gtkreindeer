/*
	This file is part of GDK-Reindeer.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GDK_REN_DRAWABLE_H
#define GDK_REN_DRAWABLE_H

#include <gdk/gdkrentypes.h>

#include <glib-object.h>
#include <gdk/gdk.h>

G_BEGIN_DECLS

#define GDK_REN_TYPE_DRAWABLE\
	(gdk_ren_drawable_get_type ())
#define GDK_REN_DRAWABLE(obj)\
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), GDK_REN_TYPE_DRAWABLE, GdkRenDrawable))
#define GDK_REN_DRAWABLE_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_CAST ((klass), GDK_REN_TYPE_DRAWABLE, GdkRenDrawableClass))
#define GDK_REN_IS_DRAWABLE(obj)\
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GDK_REN_TYPE_DRAWABLE))
#define GDK_REN_IS_DRAWABLE_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_TYPE ((klass), GDK_REN_TYPE_DRAWABLE))
#define GDK_REN_DRAWABLE_GET_CLASS(obj)\
	(G_TYPE_INSTANCE_GET_CLASS ((obj), GDK_REN_TYPE_DRAWABLE, GdkRenDrawableClass))

typedef struct _GdkRenDrawableClass GdkRenDrawableClass;

struct _GdkRenDrawable
{
	GdkDrawable parent;
};

struct _GdkRenDrawableClass
{
	GdkDrawableClass parent;

	void (* swap_buffers) (GdkRenDrawable *drawable);
};

GType
gdk_ren_drawable_get_type (void);
void
gdk_ren_drawable_swap_buffers (GdkRenDrawable *drawable);


G_END_DECLS

#endif
